# Readme
Sends event if the yarn-cluster is empty/free to trigger other stuff (e.g. send you an email or start another application)

The HadoopYarnClusterChecker-Class checks your cluster if there is any application run on it. In case your cluster is free it fires an event.

## how it works:
* it's false positive: if there is any error like it can not connect to your yarn-rest-api it sends the event.
* algorithm:
  * receive data from: "/ws/v1/cluster/apps"-REST endpoint
  * are there applications with the state "running"?
    * Yes: Is this application excluded?
    * Yes: Are there more running applications?
      * No: send the event
      * Yes: sleep for the intervall & start with the first bulletin-point
## How to use it:

```python
import hadoop-yarn-cluster-checker-lib

checker = HadoopYarnClusterChecker(server=args.server, path=args.path, intervall=args.intervall, log=args.log,
                                                               excludes=args.excludes)
checker.on_empty_cluster += event_empty
```

## Cheatsheet for me
### Twine
only works with: twine upload --repository-url https://test.pypi.org/legacy/ dist/*
